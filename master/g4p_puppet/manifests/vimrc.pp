class g4p_puppet::vimrc {

	$vim_dirs = [ "/home/.vim", "/home/.vim/ftdetect", "/home/.vim/after", "/home/.vim/after/syntax" ]

	file { $vim_dirs:
		ensure		=> directory,
		owner		=> root,
		group		=> root,
		mode		=> 755,
	}

	file { "/home/.vim/ftdetect/robot.vim":
		ensure      => file,
		owner		=> root,
		group		=> root,
		mode		=> 644,
		source		=> "puppet:///modules/g4p_puppet/g4p.robot.vim-ftdetect",
		require		=> File[$vim_dirs],
	}

	file { "/home/.vim/after/syntax/robot.vim":
		ensure      => file,
		owner       => root,
		group       => root,
		mode        => 644,
		source      => "puppet:///modules/g4p_puppet/g4p.robot.vim-after_syntax",
		require     => File[$vim_dirs],
	}

	file { "/home/.vimrc":
        ensure      => file,
        owner       => root,
        group       => root,
        mode        => 644,
        source      => "puppet:///modules/g4p_puppet/g4p.vimrc",
        require     => File[$vim_dirs],
    }

}
