class g4p_puppet {
    include g4p_puppet::slogan     # for demo 1 - slogan
    include g4p_puppet::vimrc    # for demo 2 - vimrc
    include g4p_puppet::pip_easyinstall    # for demo 3 - pip conf
}
