class g4p_puppet::pip_easyinstall {

	file { "/home/.pip":
		ensure		=> directory,
		owner		=> root,
		group		=> root,
		mode		=> 755,
	}

	file { "/home/.pip/pip.conf":
		ensure          => file,
		owner		=> root,
		group		=> root,
		mode		=> 644,
		content		=> template("g4p_puppet/pip.conf.erb"),
	}
}
